var serverData = [];
var fs = require('fs');
function trimHTML(html) {
    var div = document.createElement("div");
    div.innerHTML = html;
    return (div.textContent || div.innerText || "").trim();
}

var casper = require('casper').create({   
    verbose: true, 
    logLevel: 'debug',
    pageSettings: {
         loadImages:  false,         // The WebPage instance used by Casper will
         loadPlugins: false,         // use these settings
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

    var url = "https://panel.cloudatcost.com/login.php";

    casper.start(url, function () {
        if (this.exists('form#login-form')) {
            console.log("Logging In");
            this.fill('form#login-form', { 
                username: 'cc_username@domain.com', 
                password:  'cc_password'
            }, true);
        }
    });

    casper.then(function() {
        this.waitForSelector('div.panel-default');
    });

    casper.then(function() {
        var servers = casper.evaluate(function() {
            bt = document.querySelectorAll('.btn-info');
            var nodeArray = [];
            for (var i = 0; i < bt.length; ++i) {
                nodeArray[i] = bt[i].dataset.content;
            }

            return nodeArray;
        });

        this.each(servers, function(self, server) {
            var entry = {};
            rows = server.match(/<tr[\s\S]*?<\/tr>/g);
            this.each(rows, function(self,row){
                data = row.match(/<td[\s\S]*?<\/td>/g);
                    if (data[0].toLowerCase().indexOf("ip address") > 0 ) {
                        entry.ipAddress = trimHTML(data[1]);
                    } else if (data[0].toLowerCase().indexOf("password") > 0 ) {
                        entry.password = trimHTML(data[1]);
                    }
            });
            serverData.push(entry)

        });
        console.log(JSON.stringify(serverData));
        var inventory = "";
        this.each(serverData, function(self, data) {
            inventory += data.ipAddress + " ansible_ssh_user=root ansible_ssh_pass=" + data.password + "\n"; 
        });
        console.log(inventory);
        fs.write('inventory', inventory, 'w');

    });

    
    casper.run();
